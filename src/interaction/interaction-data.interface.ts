import { InteractionInterface } from "./interaction.interface";

export interface InteractionDataInterface {
  interaction: InteractionInterface;
}
