import { MessageInterface } from "@cryptexlabs/codex-data-model";
import { InteractionDataInterface } from "./interaction-data.interface";

export interface InteractionMessageInterface<T extends InteractionDataInterface>
  extends MessageInterface<T> {
  data: T;
}
