/**
 * A single phone number cannot be used to interact with all users because any given phone number has sending rate limits associated with it
 * by carriers. Therefore a single phone number can be used per country as the phone number that users first interact with. After the user
 * sends the initial message we will send them a message from another phone number that the user will use to interact with the
 * time rocket assistant. So when a user first interacts with us by sending us a message on a phone number that is on a marketing website for
 * example the channel will be 'ENTRYPOINT'. After a phone number is assigned to the user that assigned phone number will have a channel of
 * 'ASSIGNED'
 */
export enum PhoneNumberChannelEnum {
  /**
   * The very first phone number that a user will interact with
   */
  ENTRY_POINT = "ENTRY_POINT",

  /**
   * A phone number that has been assigned as the timerocket phone number that a user will interact with
   */
  ASSIGNED = "ASSIGNED",
}
