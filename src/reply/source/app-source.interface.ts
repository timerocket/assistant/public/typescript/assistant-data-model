import { SourceInterface } from "./source.interface";
import { AppDevice } from "../../app";

export interface AppSourceInterface<T extends AppDevice>
  extends SourceInterface {
  device: T;

  /**
   * This is null if the user hasn't logged in.
   * Users can send messages in apps without logging in as per requirement by Apple
   * UUID V4
   */
  userId: string | null;
}
