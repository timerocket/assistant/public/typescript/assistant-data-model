import { InteractionDataInterface } from "../interaction";
import { InteractionUnassignedReplyDataInterface } from "./interaction-unassigned-reply-data.interface";
import { ReplyDataType } from "./reply-data.type";

/**
 * After we know the interaction id and interaction category a reply will have a known interaction associated with it
 */
export interface ReplyDataInterface<T extends ReplyDataType>
  extends InteractionUnassignedReplyDataInterface<T>,
    InteractionDataInterface {}
