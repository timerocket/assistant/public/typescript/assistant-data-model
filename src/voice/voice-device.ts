import { SourceDestinationTypeEnum } from "../source-destination-type.enum";
import { Device } from "../device";

export interface VoiceDevice extends Device {
  /**
   * Unique device id for voice device
   */
  id: string;

  type:
    | SourceDestinationTypeEnum.VOICE_APPLE
    | SourceDestinationTypeEnum.VOICE_GOOGLE
    | SourceDestinationTypeEnum.VOICE_ECHO;
}
