export enum CommunicationTypeEnum {
  TEXT = "text",
  CARD = "card",
  CARD_LIST = "card.list",
}
