import { CardTypeEnum } from "./card-type-enum";
import { CardType } from "./card-type";

export interface CardInterface<T extends CardType> {
  id: string;
  data: T;
  type: CardTypeEnum;
}
