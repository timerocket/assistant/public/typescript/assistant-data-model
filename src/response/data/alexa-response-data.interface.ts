import { ResponseDataInterface } from "./response-data.interface";

export interface AlexaResponseDataInterface extends ResponseDataInterface {}
