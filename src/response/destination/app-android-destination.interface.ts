import { AppAndroidDevice } from "../../app";

export interface AppAndroidDestinationInterface {
  device: AppAndroidDevice;
}
