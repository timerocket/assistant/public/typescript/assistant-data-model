import { VoiceGoogleDevice } from "../../voice";

export interface VoiceGoogleResponseDestinationInterface {
  device: VoiceGoogleDevice;
}
