import { SourceDestinationTypeEnum } from "../source-destination-type.enum";
export declare type AppDeviceType = SourceDestinationTypeEnum.APP_ANDROID | SourceDestinationTypeEnum.APP_IOS | SourceDestinationTypeEnum.APP_WEB;
