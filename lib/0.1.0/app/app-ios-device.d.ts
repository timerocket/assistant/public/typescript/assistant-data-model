import { AppDevice } from "./app-device";
import { SourceDestinationTypeEnum } from "../source-destination-type.enum";
export interface AppIosDevice extends AppDevice {
    type: SourceDestinationTypeEnum.APP_IOS;
}
