import { Device } from "../device";
import { AppDeviceType } from "./app-device-type";
export interface AppDevice extends Device {
    id: string;
    type: AppDeviceType;
}
