import { ReplyDataType } from "./reply-data.type";
import { CommunicationTypeEnum } from "../communication";
export interface InteractionUnassignedReplyDataInterface<T extends ReplyDataType> {
    source: T;
    text: string;
    messageId: string;
    type: CommunicationTypeEnum;
}
