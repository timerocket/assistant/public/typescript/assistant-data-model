"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhoneNumberChannelEnum = void 0;
var PhoneNumberChannelEnum;
(function (PhoneNumberChannelEnum) {
    PhoneNumberChannelEnum["ENTRY_POINT"] = "ENTRY_POINT";
    PhoneNumberChannelEnum["ASSIGNED"] = "ASSIGNED";
})(PhoneNumberChannelEnum = exports.PhoneNumberChannelEnum || (exports.PhoneNumberChannelEnum = {}));
