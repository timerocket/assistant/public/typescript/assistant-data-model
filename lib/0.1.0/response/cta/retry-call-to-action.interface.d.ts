import { CallToActionInterface } from "./call-to-action.interface";
export interface RetryCallToActionInterface extends CallToActionInterface {
    text: string;
}
