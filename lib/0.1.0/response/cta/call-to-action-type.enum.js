"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallToActionTypeEnum = void 0;
var CallToActionTypeEnum;
(function (CallToActionTypeEnum) {
    CallToActionTypeEnum["EMAIL"] = "EMAIL";
    CallToActionTypeEnum["SMS"] = "SMS";
    CallToActionTypeEnum["INTERACTIVE"] = "INTERACTIVE";
    CallToActionTypeEnum["MOBILE_MESSENGER"] = "MOBILE_MESSENGER";
    CallToActionTypeEnum["RETRY"] = "RETRY";
    CallToActionTypeEnum["LINK"] = "LINK";
})(CallToActionTypeEnum = exports.CallToActionTypeEnum || (exports.CallToActionTypeEnum = {}));
