import { CallToActionInterface } from "./call-to-action.interface";
export interface SmsCallToActionInterface extends CallToActionInterface {
    text: string;
    link: string;
}
