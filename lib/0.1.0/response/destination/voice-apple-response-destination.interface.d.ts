import { VoiceAppleDevice } from "../../voice";
export interface VoiceAppleResponseDestinationInterface {
    device: VoiceAppleDevice;
}
