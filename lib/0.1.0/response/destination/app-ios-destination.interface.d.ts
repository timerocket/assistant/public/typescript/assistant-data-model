import { AppIosDevice } from "../../app";
export interface AppIosDestinationInterface {
    device: AppIosDevice;
}
