import { VoiceEchoDevice } from "../../voice";
export interface VoiceEchoResponseDestinationInterface {
    device: VoiceEchoDevice;
}
