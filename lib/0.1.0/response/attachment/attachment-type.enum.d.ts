export declare enum AttachmentTypeEnum {
    VIDEO = "VIDEO",
    IMAGE = "IMAGE",
    DOCUMENT = "DOCUMENT"
}
