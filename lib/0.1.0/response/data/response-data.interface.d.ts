import { CallToActionInterface } from "../cta";
import { ResponseDestinationInterface } from "../destination";
import { InteractionDataInterface } from "../../interaction";
import { CommunicationTypeEnum } from "../../communication";
import { CommunicationDataType } from "../../communication";
export interface ResponseDataInterface extends InteractionDataInterface {
    text: string;
    ctas: CallToActionInterface[];
    destination: ResponseDestinationInterface;
    messageId: string;
    type: CommunicationTypeEnum;
    data?: CommunicationDataType;
}
