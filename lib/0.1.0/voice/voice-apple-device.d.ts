import { SourceDestinationTypeEnum } from "../source-destination-type.enum";
import { VoiceDevice } from "./voice-device";
export interface VoiceAppleDevice extends VoiceDevice {
    type: SourceDestinationTypeEnum.VOICE_APPLE;
}
