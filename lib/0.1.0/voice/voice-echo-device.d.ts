import { SourceDestinationTypeEnum } from "../source-destination-type.enum";
import { VoiceDevice } from "./voice-device";
export interface VoiceEchoDevice extends VoiceDevice {
    type: SourceDestinationTypeEnum.VOICE_ECHO;
}
