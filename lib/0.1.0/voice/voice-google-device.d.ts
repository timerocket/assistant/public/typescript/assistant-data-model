import { SourceDestinationTypeEnum } from "../source-destination-type.enum";
import { VoiceDevice } from "./voice-device";
export interface VoiceGoogleDevice extends VoiceDevice {
    type: SourceDestinationTypeEnum.VOICE_GOOGLE;
}
