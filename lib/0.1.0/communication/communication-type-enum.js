"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommunicationTypeEnum = void 0;
var CommunicationTypeEnum;
(function (CommunicationTypeEnum) {
    CommunicationTypeEnum["TEXT"] = "text";
    CommunicationTypeEnum["CARD"] = "card";
    CommunicationTypeEnum["CARD_LIST"] = "card.list";
})(CommunicationTypeEnum = exports.CommunicationTypeEnum || (exports.CommunicationTypeEnum = {}));
