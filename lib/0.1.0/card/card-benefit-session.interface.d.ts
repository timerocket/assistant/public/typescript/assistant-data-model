import { SavedCardInterface } from "./card-saved.interface";
export interface CardBenefitSessionInterface extends SavedCardInterface {
    title: string;
    description: string;
    sessionCount: number;
    sessionsComplete: number;
}
