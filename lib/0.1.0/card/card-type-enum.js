"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardTypeEnum = void 0;
var CardTypeEnum;
(function (CardTypeEnum) {
    CardTypeEnum["BENEFITS_SESSIONS"] = "card.benefit.session";
})(CardTypeEnum = exports.CardTypeEnum || (exports.CardTypeEnum = {}));
