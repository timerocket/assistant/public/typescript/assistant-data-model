# Data Models
Shared data models for Assistant applications for both front end and backend applications

## Example
```json
{
  "meta": {
    "type": "na.http.healthz",
    "schemaVersion": "0.1.0",
    "correlationId": "e240c16d-dc2a-42d1-804e-1403c84c89bd",
    "time": {
      "created": "2020-09-20T20:18:42.896Z",
      "started": "2020-09-20T20:18:42.896Z"
    },
    "context": {
      "category": "default",
      "id": "none"
    },
    "locale": {
      "language": "eng",
      "country": "US"
    },
    "client": {
      "id": "08ab0dfb-849a-48a3-b22e-01b6fd1c49bf",
      "name": "support-user-service",
      "variant": "docker",
      "version": "0.3.2"
    }
  },
  "data": {
    "text": "There's a placed called Vistio just 5 miles away. They have vegan options",
    "ctas": [
      {
        "type": "INTERACTIVE",
        "text": "Will that work?",
        "optionId": "9f90fa53-3dd3-487a-b05d-072b4f8515b9",
        "image": "https://lh5.googleusercontent.com/p/AF1QipN7KcAKWRSA1TgApTva_sGYL-_RGE12Tt5kdFrK=w423-h240-k-no"
      },
      {
        "type": "retry",
        "text": "Or would you like something else?"
      }
    ],
    "destination": {
      "type": "VOICE_ALEXA",
      "deviceId": "3a0fe36c-03bd-4fa1-9d59-b3f0991f5d10"
    }
  }
}
```

## In scope
- Plain interfaces and classes that *could* be used by both front end and backend application

## Out of scope
- Interfaces or classes that *can* only be used by a front end or backend application
- Dependencies